#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "hashset.h"

void hashset_init(struct hashset *set, size_t capacity,
    float load_factor, float resize_factor,
    _Bool (*equals)(hashcode *h1, hashcode *h2))
{
    set->nodes = hashset_alloc(sizeof(hashcode *) * capacity);
    if (!set->nodes) { hashset_alloc_fail(set->nodes); }
    memset(set->nodes, HASHSET_NODE_FREE, capacity);
    set->capacity = capacity;
    set->size = 0;
    set->load_factor = load_factor;
    set->resize_factor = resize_factor;
    set->equals = equals;
}

void hashset_resize(struct hashset *set) {
    hashcode **start_new = set->nodes + set->capacity;

    size_t new_capacity = set->capacity * set->resize_factor;
    set->nodes = hashset_realloc(set->nodes, new_capacity * sizeof(hashcode *));
    memset(set->nodes + set->capacity, HASHSET_NODE_FREE,
        new_capacity - set->capacity);
    set->capacity = new_capacity;

    struct _hash_list {
        hashcode *code;
        struct _hash_list *next;
    };

    struct _hash_list *hash_list = NULL;

    // TODO: Check if this loop works
    for (size_t i = 0, j = 0; i < set->capacity && j < set->size; i++) {
        if (!hashset_is_occupied(set, i)) { continue; }
        j++;
        size_t index = *set->nodes[i] % set->capacity;
        if (index == i) { continue; }
        if (set->nodes[index] != HASHSET_NODE_FREE) {
            struct _hash_list *tmp = hashset_alloc(sizeof(struct _hash_list));
            tmp->code = set->nodes[index];
            tmp->next = hash_list;
            hash_list = tmp;
        }
        set->nodes[index] = set->nodes[i];
        set->nodes[i] = HASHSET_NODE_FREE;
    }

    while (hash_list != NULL) {
        struct _hash_list *pos = hash_list;
        hash_list = hash_list->next;
        size_t index = *pos->code % set->capacity;
        while(hashset_is_occupied(set, index)) {
            index = hashset_next_pos(set, index);
        }
        set->nodes[index] = pos->code;
        hashset_dealloc(pos);
    }
}

void hashset_insert(struct hashset *set, hashcode *code) {
    if (set->load_factor * set->capacity <= set->size) {
        hashset_resize(set);
    }
    size_t index = hashset_index(set, *code);
    while(hashset_is_occupied(set, index)) {
        if (*set->nodes[index] == *code &&
            set->equals(set->nodes[index], code)) {
            return; // Already exists in the set
        }
        index = hashset_next_pos(set, index);
    }
    set->nodes[index] = code;
    set->size++;
}

hashcode **hashset_find(struct hashset *set, hashcode *code) {
    size_t index = hashset_index(set, *code);
    size_t end = index > 0 ? index - 1 : set->capacity - index - 1;
    while (hashset_is_occupied(set, index)) {
        if (*set->nodes[index] == *code &&
            set->equals(set->nodes[index], code)) {
            return &set->nodes[index];
        }
        if (index == end) { break; }
        index = hashset_next_pos(set, index);
    }
    return NULL;
}

hashcode *hashset_remove(struct hashset *set, hashcode **hash_ptr) {
    if (!hash_ptr || !*hash_ptr) { return NULL; }
    hashcode *tmp = *hash_ptr;
    *hash_ptr = NULL;
    return tmp;
}

void hashset_free(struct hashset *set) {
    hashset_dealloc(set->nodes);
    set->size = 0;
    set->capacity = 0;
}

hashcode **hashset_first(struct hashset *set) {
    hashcode **first = set->nodes;
    hashcode **last = &set->nodes[(set)->capacity - 1];
    while(!*first && first <= last) { first++; }
    if (!*first && first == last) { return NULL; }
    return first;
}

hashcode **hashset_next(struct hashset *set, hashcode **current) {
    hashcode **last = &set->nodes[set->capacity - 1];
    do {
        current++;
    } while(current <= last && !*current);
    if (current > last || !*current) { return NULL; }
    return current;
}