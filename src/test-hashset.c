#include <stdio.h>
#include <stdlib.h>

#include "hashset.h"

struct vector3 {
    float x, y, z;
    hashcode hash;
};

hashcode vector3_hash(struct vector3 *vec) {
    return 3 * vec->x + 5 * vec->y + 7 * vec->z;
}

void vector3_hashset_insert(struct hashset *set, struct vector3 *vec) {
    vec->hash = vector3_hash(vec);
    hashset_insert(set, &vec->hash);
}

_Bool vector3_hashset_equals(hashcode *h1, hashcode *h2) {
    struct vector3 *v1 = hashset_entry(h1, struct vector3, hash);
    struct vector3 *v2 = hashset_entry(h2, struct vector3, hash);
    return v1->x == v2->x && v1->y == v2->y && v1->z == v2->z;
}

struct vector3 *vector3_hashset_find(struct hashset *set, struct vector3 *vec) {
    vec->hash = vector3_hash(vec);
    hashcode **result = hashset_find(set, &vec->hash);
    return result ? hashset_entry(*result, struct vector3, hash) : NULL;
}

int main() {
    struct hashset set;
    hashset_init(&set, 10, 1.0, 2.0, vector3_hashset_equals);

    for (int i = 0; i < 10; i++) {
        struct vector3 *v = malloc(sizeof(struct vector3));
        v->z = (v->y = (v->x = i) + 1) + 1;
        vector3_hashset_insert(&set, v);
    }

    for (size_t i = 0; i < set.capacity; i++) { printf("%x ", set.nodes[i]); }
    printf("\n");

    hashcode **pos;
    struct vector3 *x;
    hashset_for_each_entry(pos, x, &set, hash) {
        printf("%f %f %f\n", x->x, x->y, x->z);
    }

    printf("Check if hashset contains vector (%%f %%f %%f): ");
    struct vector3 vec;
    scanf("%f%f%f", &vec.x, &vec.y, &vec.z);

    printf("%s\n", vector3_hashset_find(&set, &vec) ? "Yes" : "No");

    hashset_for_each(pos, &set) {
        hashset_remove(&set, pos);
    }

    for (size_t i = 0; i < set.capacity; i++) { printf("%x ", set.nodes[i]); }
    printf("\n");

    hashset_free(&set);

    printf("-- testing set operations --\n");

    struct hashset a, b, c, d;

    hashset_init(&a, 3, 1.0, 2.0, vector3_hashset_equals); 
    hashset_init(&b, 3, 1.0, 2.0, vector3_hashset_equals); 
    hashset_init(&c, 3, 1.0, 2.0, vector3_hashset_equals); 
    hashset_init(&d, 3, 1.0, 2.0, vector3_hashset_equals); 

    vector3_hashset_insert(&a, &(struct vector3) { 1.0, 2.0, 3.0 });
    vector3_hashset_insert(&a, &(struct vector3) { 4.0, 5.0, 6.0 });
    vector3_hashset_insert(&b, &(struct vector3) { 4.0, 5.0, 6.0 });
    vector3_hashset_insert(&b, &(struct vector3) { 7.0, 8.0, 9.0 });
    vector3_hashset_insert(&c, &(struct vector3) { 0.0, 0.0, 0.0 });

    // D <- A sym B U C
    hashset_symdiff(&d, &a, &b);
    hashset_union(&d, &d, &c);

    hashset_for_each_entry(pos, x, &d, hash) {
        printf("%f %f %f\n", x->x, x->y, x->z);
    }

    return 0;
}