#ifndef __HASHSET_H__
#define __HASHSET_H__

#include <stdlib.h>
#include "compiler.h"

typedef unsigned long hashcode;

#define HASHSET_NODE_FREE     0

struct hashset {
    hashcode **nodes;
    size_t capacity;
    size_t size;
    float resize_factor;
    float load_factor;
    _Bool (*equals)(hashcode *h1, hashcode *h2);
};

#ifndef hashset_alloc
#define hashset_alloc(x) malloc(x)
#endif

#ifndef hashset_realloc
#define hashset_realloc(x, newm) realloc(x, newm)
#endif

#ifndef hashset_dealloc
#define hashset_dealloc(x) free(x)
#endif

#ifndef hashset_alloc_fail
#define hashset_alloc_fail(x)
#endif

#define hashset_is_occupied(set, index) \
    ((set)->nodes[index] != HASHSET_NODE_FREE)

#define hashset_index(set, hashcode) ((hashcode) % (set)->capacity)

#define hashset_next_pos(set, pos) hashset_index((set), (pos) + 1)

#define hashset_position_occupied(set, pos) ((set)->nodes[pos] != 0)

void hashset_init(struct hashset *set,
    size_t capacity, float load_factor,
    float resize_factor, _Bool (*equals)(hashcode *h1, hashcode *h2));

void hashset_resize(struct hashset *set);

void hashset_insert(struct hashset *set, hashcode *code);

hashcode **hashset_find(struct hashset *set, hashcode *code);

hashcode *hashset_remove(struct hashset *set, hashcode **hash_ptr);

void hashset_free(struct hashset *set);

hashcode **hashset_first(struct hashset *set);

hashcode **hashset_next(struct hashset *set, hashcode **current);

#define hashset_entry(ptr, type, member) container_of(ptr, type, member)

#define hashset_for_each(pos, set) \
    for (pos = hashset_first(set); \
        pos; \
        pos = hashset_next(set, pos))

#define hashset_for_each_entry(pos, t, set, member) \
    for (pos = hashset_first(set), \
        t = pos ? hashset_entry(*pos, __typeof__(*t), member) : NULL; pos; \
        pos = hashset_next(set, pos), \
        t = pos ? hashset_entry(*pos, __typeof__(*t), member) : NULL)

static inline void
hashset_union(struct hashset *into, struct hashset *a, struct hashset *b) {
    hashcode **item;
    hashset_for_each(item, a) { hashset_insert(into, *item); }
    hashset_for_each(item, b) { hashset_insert(into, *item); }
}

static inline void
hashset_intersect(struct hashset *into, struct hashset *a, struct hashset *b) {
    hashcode **item;
    hashset_for_each(item, a) {
        if (hashset_find(b, *item)) { hashset_insert(into, *item); }
    }
}

static inline void
hashset_diff(struct hashset *into, struct hashset *a, struct hashset *b) {
    hashcode **item;
    hashset_for_each(item, a) {
        if (!hashset_find(b, *item)) { hashset_insert(into, *item); }
    }
}

static inline void
hashset_symdiff(struct hashset *into, struct hashset *a, struct hashset *b) {
    hashset_diff(into, a, b);
    hashset_diff(into, b, a);
}

#endif