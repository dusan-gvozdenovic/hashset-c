# hashset-c

This is intended to be a fast linux-style generic hashset implemented in C.

# Specs
- Open addressing
- Linear probing

# Examples

```c
    struct hashset set;
    // Initialize set with capacity, load_factor and scaling_factor
    hashset_init(&set, 100, 0.75, 2.0);

    // Iterate through set
    hashcode **pos;
    struct vector3 *vec;
    hashset_for_each_entry(pos, vec, set, hash) {
        printf("%f %f %f\n", vec->x, vec->y, vec->z);
    }

    //Destroy hashset
    hashset_destroy(&set);
```